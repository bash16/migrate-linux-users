#!/bin/bash

# Copyright (c)2010-2012 rbTechnologies, LLC
# By Rubin Bennett <rbennett@rbtechvt.com>

# Released under the terms and conditions of the GNU Public License version 2.


# A simple script to assist in server migrations from Linux to Linux
# Intended to be run on the NEW server, and expecting that you have performed
# ssh key exchange for password-less login to the OLD server.

# IP address or hostname of source server (e.g. server your're migrating
# AWAY from
sourceServer=192.168.154.139

function syncusers() {
echo -n "Backup your existing passwd files? [Y/N] "
read
if [[ "$REPLY" =~ [yY](es)* ]]
then
        cp -v /etc/passwd /etc/passwd.bak
        cp -v /etc/group /etc/group.bak
        cp -v /etc/shadow /etc/shadow.bak
fi
        echo -e "\nCopying files:\n"
        scp $sourceServer:/etc/passwd /tmp/passwd.$sourceServer
        scp $sourceServer:/etc/group /tmp/group.$sourceServer
        scp $sourceServer:/etc/shadow /tmp/shadow.$sourceServer

        # First, make a list of non-system users that need to be moved.

        export UGIDLIMIT=500
        awk -v LIMIT=$UGIDLIMIT -F: '($3 >= LIMIT) && ($3 != 65534)' /tmp/passwd.$sourceServer > /tmp/passwd.mig
        awk -v LIMIT=$UGIDLIMIT -F: '($3 >= LIMIT) && ($3 != 65534)' /tmp/group.$sourceServer >/tmp/group.mig
        awk -v LIMIT=$UGIDLIMIT -F: '($3 >= LIMIT) && ($3 != 65534) { print $1 }' /tmp/passwd.$sourceServer \
 | tee - | egrep -f - /tmp/shadow.$sourceServer > /tmp/shadow.mig

#echo -e "Copying user accounts and passwords\n"
        # Now copy non-duplicate entries in to the new server files...
        echo -e "\nCopying users:\n"
        while IFS=: read user pass uid gid full home shell
        do
                line="$user:$pass:$uid:$gid:$full:$home:$shell"
                exists=$(grep $user /etc/passwd)
                if [ -z "$exists" ]
                then
                        echo "Copying entry for user $user to new system"
                        echo $line >> /etc/passwd
                fi
        done </tmp/passwd.mig

        echo -e "\nCopying groups:\n"
        while IFS=: read group pass gid userlist
        do
                line="$group:$pass:$gid:$userlist"
                exists=$(grep $group /etc/group)
                if [ -z "$exists" ]
                then
                        echo "Copying entry for group $group to new system"
                        echo $line >> /etc/group
                fi
        done </tmp/group.mig

        echo -e "\nCopying passwords:\n"
        while IFS=: read user pass lastchanged minimum maximum warn
        do
                line="$user:$pass:$lastchanged:$minimum:$maximum:$warn"
                exists=$(grep $user /etc/shadow)
                if [ -z "$exists" ]
                then
                        echo "Copying entry for user $user to new system"
                        echo $line >> /etc/shadow
                fi
        done </tmp/shadow.mig
       #fi

}

syncusers
echo -e "\nIf you use Samba, you may need to manually move the machines group with GID 200..."

exit 0
